<?php
/*
Plugin Name: FormsValidator
Plugin URI:   https://developer.wordpress.org/plugins/formsvalidator/
Description:  Forms management, mail sending, and data storage, with Timber and ACF
Version:      0.9.1
Text Domain:  formsvalidator
Domain Path:  /languages/
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


define( 'FORMSVALIDATOR_URL', plugin_dir_url ( __FILE__ ) );
define( 'FORMSVALIDATOR_DIR', plugin_dir_path( __FILE__ ) );

// Activation, uninstall
register_activation_hook( __FILE__, 'FormsValidator_Install' );
register_deactivation_hook ( __FILE__, 'FormsValidator_Uninstall' );

function FormsValidator_Install()
{
	global $wpdb;

	$table_name = $wpdb->prefix . 'emailarchives';	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	  `id` int(11) NOT NULL,
	  `id_form` int(11) NOT NULL,
	  `email` varchar(100) NOT NULL,
	  `datereception` datetime NOT NULL,
	  `json` text NOT NULL
	) $charset_collate;";
	$sql.= "ALTER TABLE $table_name 
	ADD PRIMARY KEY (`id`);";
	$sql.= "ALTER TABLE $table_name 
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

}

// Load translations
load_plugin_textdomain ( 'formsvalidator', false, basename(rtrim(dirname(__FILE__), '/')) . '/languages' );

function formsvalidator_acf_add_local_field_groups()
{
	include(FORMSVALIDATOR_DIR."fields.php");
}
add_action('acf/init', 'formsvalidator_acf_add_local_field_groups');




//echo "path : ".FORMSVALIDATOR_DIR. 'classes/class_formsvalidator.php';die;
require_once( FORMSVALIDATOR_DIR. 'classes/class_formvalidator.php');
require_once( FORMSVALIDATOR_DIR. 'classes/class_formsvalidator.php');
$forms=FormsValidator::initFormsValidator();
add_action('init', array(&$forms,'formsvalidator_init'));

