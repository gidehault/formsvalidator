<?php

/**
 * Representing one form, with his attributes
 */
class FormValidator
{
  var $form_name;
  var $template_name;
  var $recaptcha;
  var $secret_recaptcha;
  static $rendered=false;
  public function __construct($form_name,$template_name)
  {
      $this->form_name=$form_name;
      $this->template_name=$template_name;
      $this->recaptcha=get_field('invisible_recaptcha', 'option');
      $this->secret_recaptcha=get_field('invisible_recaptcha_secret', 'option');
      if(!FormValidator::$rendered)
      {
        FormValidator::$rendered=true;
        add_action('wp_footer', array( $this, 'render_once' ) );
      }
      add_action('wp_footer', array( $this, 'render' ) );
      add_filter('clean_url', array( $this, 'formsvalidator_strip_ampersand'), 99, 3);

  }
  function formsvalidator_strip_ampersand($url, $original_url, $_context) {
      if (strstr($url, "google.com") !== false) {
          $url = str_replace("&#038;", "&", $url);
      }
      return $url;
  }
  public function render_once()
  {
    wp_enqueue_script('formsvalidator-core', FORMSVALIDATOR_URL . 'assets/formsvalidator.js', array('jquery'), '1.0', true);
    $inline = 'var formsvalidator_url="' . FORMSVALIDATOR_URL . '";';
    wp_add_inline_script('formsvalidator-core', $inline, 'after');
    if($this->recaptcha)
    { 
      wp_enqueue_script('formsvalidator-recaptcha', 'https://www.google.com/recaptcha/api.js?onload=formsvalidator_recaptcha_onloadCallback&render=explicit', array('formsvalidator-core'), '1.0', true);
      $inline=$inline.'var formsvalidator_recaptcha_onloadCallback = function() {'
        . 'grecaptcha.render("formsvalidator_recaptcha_submit_'.$this->template_name.'", {'
        . '"sitekey" : "' . $this->recaptcha . '",'
        . '"size" : "invisible",'
        . '"callback" : "formsvalidator_recaptcha_onSubmit"'
        . '});'
        . '};';
      wp_add_inline_script('formsvalidator-recaptcha', $inline, 'before');
    }
    wp_enqueue_style( "formsvalidator-loading", FORMSVALIDATOR_URL."assets/loading.css", array() );
    wp_enqueue_style( "formsvalidator-loading", FORMSVALIDATOR_URL."assets/ldbtn.min.css", array() );
}
  public function render() {
    $inline="";
    //$inline. = 'jQuery( document ).ready(function() {';
    $inline.='fn_formsvalidator("' . get_site_url() . '","' . $this->form_name . '","' . $this->template_name . '","' . get_the_ID() . '");';
    //$inline.='});';
    wp_add_inline_script('formsvalidator-core', $inline, 'after');
  }
}
