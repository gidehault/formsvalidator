<?php

class FormsValidator
{

    private static $singleton=null;

    public function formsvalidator_init()
    {
        add_action('wp_ajax_formvalidator_sendmail', array(&$this,'ajax_sendmail'));
        add_action('wp_ajax_nopriv_formvalidator_sendmail', array(&$this,'ajax_sendmail'));

        add_action( 'admin_menu', array(&$this,'submenus') );

        register_post_type('emailform', array(
            'labels' => array(
                'name' => __('Formulaires','formsvalidator'),
                'singular_name' => __('Formulaire','formsvalidator'),
                'all_items' => __('Tous les formulaires','formsvalidator'),
                'add_new_item' => __('Ajouter un formulaire','formsvalidator'),
                'edit_item' => __('Editer le formulaire','formsvalidator'),
                'new_item' => __('Nouveau formulaire','formsvalidator'),
                'view_item' => __('Voir le formulaire','formsvalidator'),
                'search_items' => __('Rechercher parmi les formulaires','formsvalidator'),
                'not_found' => __('Pas de formulaire trouvé','formsvalidator'),
                'not_found_in_trash' => __('Pas de formulaire dans la corbeille','formsvalidator'),
            ),
            'public' => false, 
            'menu_icon' => 'dashicons-feedback',
            'capability_type' => 'post',
            'show_ui' => true,
            'exclude_from_search' => true,
            'hierarchical' => false,
            'has_archive' => false,
            'query_var' => true,
            'pages' => false,
            'show_in_nav_menus' => false,
            'supports' => array('title', 'editor', 'thumbnail', 'custom-fields')
            )
        );


    }
    public static function initFormsValidator()
    {
        if(FormsValidator::$singleton==null)
            FormsValidator::$singleton=new FormsValidator();
        return FormsValidator::$singleton;
    }

    public function submenus()
    {
        add_submenu_page(
            'edit.php?post_type=emailform',
            __('Données','formsvalidator'),
            __('Données','formsvalidator'),
            'manage_options',
            'formsvalidator_donnees',
            array(&$this,'pageDonnees')
        );

        if( function_exists('acf_add_options_page') ) {
        
            acf_add_options_sub_page(array(
                'page_title'        => __('Configuration du plugin FormsValidator','formsvalidator'),
                'menu_title'        => __('Config','formsvalidator'),
                'menu_slug' 	    => 'formsvalidatorconfig',
                'parent_slug'       => 'edit.php?post_type=emailform',
                'capability'        => 'edit_posts',
                'redirect'          => false
            ));
        }

    }
    public function pageDonnees()
    {
        include(FORMSVALIDATOR_DIR."admin/donnees.php");
    }


     private function replace($text)
     {
        foreach($_POST as $key=>$value)
        {
            $value=filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            $text=str_replace("{{".$key."}}",nl2br($value),$text);
        }
        return $text;
     }

     // construction des destinataires du mail
     // $dest peut avoir les valeurs suivantes :
     // - une ou plusieurs adresses mail, séparées par des espaces
     // - certaines de ces adresses peuvent être sous la form {{email}}, pour récupérer la valeur d'un champ saisi dans le form
     // - {{_admin}} représente l'email de l'admin défini dans WordPress
     // si $dest est vide et que allowempty est à true, on retourne l'admin défini dans WordPress
     private function build_dests($dest,$allowempty=true)
     {
        if($dest=="" && $allowempty)
            return array();
        if($dest=="" && !$allowempty)
            return array(get_bloginfo("admin_email"));
        $dest=explode(" ",$dest);
        $tab=array();
        foreach($dest as $email)
        {
            if($email=="{{_admin}}")
                $email=get_bloginfo("admin_email");
            else
                $email=$this->replace($email);
            $tab[]=$email;
        }
        return $tab;
     }

     // prise en charge de l'éventuel fichier uploadé
     public function loadFiles($formulaire)
     {
        if($_FILES=="" || sizeof($_FILES)==0)
            return null;
        $tab_total=array();
        $nomfinal="";
        $msg_erreur="";
        $keys_files=array_keys($_FILES);
        while(sizeof($keys_files)>0)
        {
            $tab=array();
            $nomchamp=array_shift($keys_files);
            if($nomchamp!="" && $_FILES[$nomchamp]["name"]!="")
            {
                $target=wp_upload_dir();
                $target_dir = $target['path']."/";
                $namefile=basename($_FILES[$nomchamp]["name"]);
                $target_file = $target_dir . $namefile;
    
                if (file_exists($target_file)) {
                    $namefile=rand(111,999).$namefile;
                    $target_file = $target_dir . $namefile;
                }
                // Check file size
                if ($_FILES[$nomchamp]["size"] > get_field("taille_maximale_fichier",$formulaire->ID)*1000000) {
                    $msg_erreur=get_field("message_erreur_taille",$formulaire->ID);
                }
                // Allow certain file formats
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                $formats=explode(" ",get_field("formats_autorises",$formulaire->ID));
                if(!in_array($imageFileType,$formats)) {
                    $msg_erreur=get_field("message_erreur_format",$formulaire->ID);
                }
    
                if($msg_erreur=="")
                {
                    if (move_uploaded_file($_FILES[$nomchamp]["tmp_name"], $target_file)) {
                        $nomfinal=$target['url']."/".$namefile;
                    } else {
                        $msg_erreur=get_field("message_erreur_upload",$formulaire->ID);
                    }
                }
            }
            //else
            //    $msg_erreur=get_field("message_erreur_upload",$formulaire->ID);
    
            $tab['nomchamp']=$nomchamp;
            $tab['nomfichier']=$nomfinal;
            $tab['msgerreur']=$msg_erreur;
            $tab_total[]=$tab;
        }
        return $tab_total;
     }
    private function error($msg="Problème !")
    {
         $tab['msg_retour']=$msg;
         wp_send_json_error($tab);
    }

    // permet d'appliquer d'éventuelles règles d'envoi
    public function loadReglesEnvoi($dest,$formulaire,$datas)
    {
      $regles=get_field("regles",$formulaire->ID);
      if($regles=="")
        return $dest;

      foreach($regles as $regle)
      {
        if(strtolower($datas[$regle['nom_champ']])==strtolower($regle['valeur_champ']))
          $dest=explode(" ",$regle['email_destinataire']);
      }
      return $dest;
    }

    // définit les divers contextes Timber et génère le html de la page
    public function loadBody($context,$post,$elements,$form,$template)
    {
        $form->sujet=Timber::compile_string($form->sujet,$elements);
        $context['post'] = $post;
        $context['form'] = $form;

        foreach($elements as $key=>$value)
        {
            $context[$key]=$value;
        }

        $http= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https://" : "http://");
        $context['_email_title']=get_field("sujet",$form->ID);
        $context['site_url']=$http.$_SERVER['HTTP_HOST'];
        $context['_site_url']=$http.$_SERVER['HTTP_HOST'];
        $context['_site_title']=get_bloginfo('name');
        $context['_admin_email']=get_bloginfo("admin_email");
        $context['_theme_url']=get_stylesheet_directory_uri();
        $context['_template']=$template;
        $templates = array( 'mail/mail_'.$template.'.twig' );

        return Timber::compile( $templates, $context );
    }

    // méthode appelée par l'appel Ajax
    public function ajax_sendmail(){
        $id_post = filter_input(INPUT_POST, 'id_post', FILTER_SANITIZE_SPECIAL_CHARS);
        $template_post = filter_input(INPUT_POST, 'template', FILTER_SANITIZE_SPECIAL_CHARS);

        if($id_post!="")
        {
            $post=get_post($id_post);
            $forms = get_posts(array(
                'numberposts'   => -1,
                'post_type'     => 'emailform',
                'meta_query'	=> array(
                    'relation'		=> 'AND',
                    array(
                        'key'	 	=> 'page_du_formulaire',
                        'value'	  	=> $id_post,
                        'compare' 	=> 'LIKE',
                    ),
                    array(
                        'key'	  	=> 'fichier_template',
                        'value'	  	=> $template_post,
                        'compare' 	=> '=',
                    ),
                ),
            ));
        }
        if($forms=="" || sizeof($forms)==0)
        {
            // looking for a page-free form
            $forms = get_posts(array(
                'numberposts'   => -1,
                'post_type'     => 'emailform',
                'meta_query'	=> array(
                    'relation'		=> 'AND',
                    array(
                        'key'	 	=> 'page_du_formulaire',
                        'value'	  	=> '',
                        'compare' 	=> '==',
                    ),
                    array(
                        'key'	  	=> 'fichier_template',
                        'value'	  	=> $template_post,
                        'compare' 	=> '=',
                    ),
                ),
    
            ));
            if($forms=="" || sizeof($forms)==0)
            {
                $this->error(__("Pas de formulaire déclaré",'formsvalidator'));
                return;
            }
        }
        $formulaire=$forms[0];
        $message_retour=get_field("message_retour",$formulaire->ID);
        $url_dest=get_field("page_dest",$formulaire->ID);

        // validation recaptcha
        if(!$this->validateRecaptcha())
        {
            $this->error(__("Problème de validation Recaptcha",'formsvalidator'));
            return;
        }

        if($message_retour=="")
            $message_retour="Message envoyé !";
        $message_retour_erreur=get_field("message_retour_erreur",$formulaire->ID);
        if($message_retour_erreur=="")
            $message_retour_erreur="Un problème est survenu";

        $form_timber=Timber::get_post($formulaire->ID);
        $post_timber=Timber::get_post($post->ID);
        $context = Timber::get_context();

        // tentative de chargement d'un fichier
        $tab_files=$this->loadFiles($formulaire);
        if($tab_files!=null && sizeof($tab_files)>0)
        {
            foreach($tab_files as $tab_file)
            {
                if($tab_file['msgerreur'])
                {
                    $this->error($tab_file['msgerreur']);
                    return;
                }
                $name=$tab_file['nomchamp'];
                $file=$tab_file['nomfichier'];
                $_POST[$name]=$file;
            }
        }

        // toute cette partie va être exécutée deux fois,
        // correspondant aux 2 mails définis dans l'admin
        $suffix="";
        $body="";
        $sanitizes=array();
        foreach($_POST as $key=>$value)
        {
            $sanitizes[$key]=filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        for($i=0;$i<2;$i++)
        {

          if($i==1)
            $suffix="_copie";

            // si le sujet n'est pas défini, on considère que le mail n'est pas défini
            $subject=$this->replace(get_field("sujet$suffix",$formulaire->ID));
            if($subject=="")
            {
                if($i==0)
                {
                    $this->error(__("Aucun sujet n'est défini",'formsvalidator'));
                    return;
                }
                break;
            }

            // chargement du template ; soit le template défini par l'appel à new FormValidator(),
            // soit par le template défini dans l'admin
            if(get_field("fichier_template$suffix",$formulaire->ID)!="")
                $template=get_field("fichier_template$suffix",$formulaire->ID);
            else
                $template=$template_post;

            if($template=="")
            {
                $this->error(__("Pas de template",'formsvalidator'));
                return;
            }

             // l'émetteur du mail est soit défini dans l'admin, soit l'email de l'admin WordPress
            $emetteur=get_field("emetteur",$formulaire->ID);
            if($emetteur=="")
                $emetteur='"'.get_bloginfo("name").'"<'.get_bloginfo("admin_email").'>';

            // les destinataires sont construits à partir de ce qui est défini dans l'admin
            $dest=$this->build_dests(get_field("destinataire$suffix",$formulaire->ID),false);
            $dest_cc=$this->build_dests(get_field("destinataire_cc$suffix",$formulaire->ID));
            $dest_cci=$this->build_dests(get_field("destinataire_cci$suffix",$formulaire->ID));

            // uniquement pour le premier mail, on lit les règles d'envoi qui vont éventuellement
            // réécrire les destinataires
            if($i==0)
              $dest=$this->loadReglesEnvoi($dest,$formulaire,$sanitizes);
            
            // hook
            // juste avant la génération du mail, on appelle un hook pour une éventuelle personnalisation
            // pour ce site
            $array_action=array("datas"=>&$sanitizes,"context"=>&$context,"post"=>&$post,"formulaire"=>&$formulaire,"form_timber"=>&$form_timber,"template"=>&$template,"dest"=>&$dest,"destcc"=>&$destcc,"destcci"=>&$destcci);
            do_action_ref_array( 'formsvalidator/before_sending_mail',array(&$array_action));
            // génération du corps HTML du mail
            $body=$this->loadBody($context,$post_timber,$sanitizes,$form_timber,$template);  

            // c'est parti, on envoie le mail
            if($body=="")
            {
               $this->error(__("Le corps du mail est vide",'formsvalidator'));
               return;
            }
            $retour=$this->sendmail($dest,$emetteur,$subject,$body,$dest_cc,$dest_cci);

            if(!$retour)
              break;
        }

        $tab=array();
        if(!$retour) // il y a eu un problème lors de l'envoi du mail
        {
            $this->error($message_retour_erreur);
            return;
        }
        else // tout va bien
        {
            $tab['msg_retour']='<span">'.$message_retour.'</span>';
            if($url_dest!="")
                $tab['url_dest']=$url_dest;

            $array_action=array("datas"=>&$sanitizes,"context"=>&$context,"post"=>&$post,"formulaire"=>&$formulaire,"form_timber"=>&$form_timber,"msg_retour"=>&$msg_retour,"body"=>&$body);
            do_action_ref_array( 'formsvalidator/after_sending_mail',array(&$array_action));

            // si tout va bien et qu'on en a fait la demande,
            // toutes les données du mail sont archivées en base
            if(get_field("archive",$formulaire->ID)[0]=="archive")
                $this->archive($formulaire->ID,$sanitizes);
        }
        wp_send_json_success($tab);

    }
    public function sendmail($to,$emetteur,$subject,$body,$dest_cc=array(),$dest_cci=array() ) {
      $headers=array();
      $headers[] = 'From: '.$emetteur;
      foreach($dest_cc as $cc)
        $headers[] = 'Cc: <'.$cc.'>';
      foreach($dest_cci as $cci)
        $headers[] = 'Cci: <'.$cci.'>';
      $headers[] = 'Content-Type: text/html; charset=UTF-8';
      return wp_mail($to,$subject,$body,$headers);
    }

    // insertion en base des données du mail
    public function archive($id_form,$datas)
    {
        global $wpdb;

        unset($datas['action']);
        unset($datas['template']);
        unset($datas['id_post']);
        $date=date("Y-m-d H:i:s");
        $email=$datas['email'];
        $json=json_encode($datas);
        $table = $wpdb->prefix.'emailarchives';
        $data = array('id_form' => $id_form, 'email'=>$email,'datereception' => $date,'json' => $json);
        $format = array('%d','%s','%s','%s');
        $ret=$wpdb->insert($table,$data,$format);

    }
    public function updateArchive($id_form,$email,$datas)
    {
        global $wpdb;
        $json=json_encode($datas);
        $table = $wpdb->prefix.'emailarchives';
        $data = array('json' => $json);
        $where = array('id_form' => $id_form, 'email'=>$email);
        $format = array('%s');
        $wpdb->update($table,$data,$where,$format);
    }
    public static function getDatas($id_form)
    {
        global $wpdb;
        $table = $wpdb->prefix.'emailarchives';
        return $wpdb->get_results("SELECT * FROM $table WHERE id_form=".$id_form);
    }
    public static function eraseDatas($id_form)
    {
        global $wpdb;
        $table = $wpdb->prefix.'emailarchives';
        $wpdb->delete( $table, array( 'id_form' => $id_form ) );
    }


    public function validateRecaptcha()
    {
        $recaptcha=get_field('invisible_recaptcha', 'option');
        $secret_recaptcha=get_field('invisible_recaptcha_secret', 'option');
  
        if($recaptcha=="" || $secret_recaptcha=="")
            return true;
        $token=filter_input(INPUT_POST, "recaptcha_token", FILTER_SANITIZE_SPECIAL_CHARS);
        $url="https://www.google.com/recaptcha/api/siteverify";
        $params=array();
        $params['secret']=$secret_recaptcha;
        $params['response']=$token;
        $response=$this->post($url,$params);
        $response=json_decode($response);
        if($response->success=="true")
            return true;
        return false;
  
    }
  
    //Transform our POST array into a URL-encoded query string.
    private function post($url, $postVars = array())
    {
        $postStr = http_build_query($postVars);
        $options = array(
        'http' =>
            array(
                'method'  => 'POST', //We are using the POST HTTP method.
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postStr //Our URL-encoded query string.
            )
        );
        $streamContext  = stream_context_create($options);
        $result = file_get_contents($url, false, $streamContext);
        if($result === false){
            $error = error_get_last();
            throw new Exception('POST request failed: ' . $error['message']);
        }
        return $result;
    }
  
}
