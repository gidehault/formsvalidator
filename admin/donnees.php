<?php 
    $id_form=filter_input(INPUT_GET, "id_form", FILTER_SANITIZE_SPECIAL_CHARS);
    if($id_form!="")
    {
        $form=get_post($id_form);
    }
    if($_GET['action']=="erase_all")
    {
        FormsValidator::eraseDatas($id_form);
        echo '<div class="updated"><p>'.__('Données supprimées','formsvalidator').' !</p></div>';
    }
?>
<h1><?=__('Accès aux données','formsvalidator')?></h1>
<h3><?=__('Sélectionnez un formulaire','formsvalidator')?></h3>
<form method="GET" action="edit.php">
	<input type="hidden" name="post_type" value="emailform" />
	<input type="hidden" name="page" value="formsvalidator_donnees" />
	<select name="id_form" onchange="this.form.submit()">
		<option value="">--<?=__('Sélectionnez un formulaire','formsvalidator')?>--</option>
<?php 
    $args = array(
        'post_type'         => 'emailform',
        'posts_per_page'    => -1,
        'orderby'           => 'title',
        'order'             => 'ASC',
		'meta_key'			=> 'archive',
		'meta_value'		=> 'archive',
		'meta_compare'		=> 'LIKE'
    );
    $posts=get_posts($args);
    foreach($posts as $form)
    {
    	$selected="";
    	if($form->ID==$id_form)
    		$selected="selected";
    	echo '<option '.$selected.' value="'.$form->ID.'">'.$form->post_title.'</option>';
    }
?>
</select>
</form>
<?php
if($id_form!="")
{
    echo '<h3>Données</h3>';
	$datas=FormsValidator::getDatas($id_form);
    if(sizeof($datas)==0) 
        echo '<h4>'.__('Aucune donnée actuellement en base','formsvalidator').'</h4>';
	else 
    {
		$tab_ref=array();
		foreach($datas as $line) { 
			$json=json_decode($line->json);
			foreach($json as $key=>$value)
				$tab_ref[$key]=$key;
		}
        $array_action=array("id_post"=>&$id_post,"keys"=>&$tab_ref);
        do_action_ref_array( 'formsvalidator/archives_header',array(&$array_action));

?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<table id="table_id" class="display">
    <thead>
        <tr>
<?php
echo "<th>Date</th>";
foreach($tab_ref as $key=>$value) {
?>
            <th><?=$value?></th>
<?php } ?>
        </tr>
    </thead>
    <tbody>
<?php
foreach($datas as $line) { 
	$json=json_decode($line->json);
    $array_action=array("id_post"=>&$id_post,"keys"=>&$tab_ref,"date"=>&$line->datereception,"datas"=>&$json);
    do_action_ref_array( 'formsvalidator/archives_dataline',array(&$array_action));

	?>
        <tr>
        	<td><?=$line->datereception?></td>
	<?php foreach($tab_ref as $key=>$value) { 
        if(is_object($json->$key))
            echo "<td>".print_r($json->$key,true)."</td>";
        else
            echo "<td>".$json->$key."</td>";
    } ?>
        </tr>
<?php } ?>
    </tbody>
</table>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>

<script>
	jQuery(document).ready( function ($) {
	    $('#table_id').DataTable({
        	dom: 'Bfrtip',
            pageLength: 20,
            language: {
                processing:     "Traitement en cours...",
                search:         "Rechercher&nbsp;:",
                lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable:     "Aucune donnée disponible dans le tableau",
                paginate: {
                    first:      "Premier",
                    previous:   "Pr&eacute;c&eacute;dent",
                    next:       "Suivant",
                    last:       "Dernier"
                }
        	},
            buttons: [
                	'csv',
                    {
                        text: '<?=__('Supprimer toutes les données','formsvalidator')?>',
                        action: function ( e, dt, node, config ) {
                            if ( confirm( "<?=__('Etes vous sûr ? Toutes les données seront supprimées','formsvalidator')?>" ) ) {
                                document.location.href='edit.php?post_type=emailform&page=formsvalidator_donnees&id_form=<?=$id_form?>&action=erase_all';
                            } else {
                            }
                        }
                    }
            	]
    });
} );
</script>
<?php } ?>
<?php } ?>
