var formsvalidator_site_url;
var formsvalidator_name;
var formsvalidator_template;
var formsvalidator_postid;
function fn_formsvalidator(formsvalidator_site_url_param,formsvalidator_name_param,formsvalidator_template_param,formsvalidator_postid_param)
{
  formsvalidator_site_url=formsvalidator_site_url_param;
  formsvalidator_name=formsvalidator_name_param;
  formsvalidator_template=formsvalidator_template_param;
  formsvalidator_postid=formsvalidator_postid_param;

  jQuery(formsvalidator_name+" #msg_feedback").hide();
  jQuery(formsvalidator_name+" :submit").addClass('ld-ext-left');
  jQuery(formsvalidator_name+" :submit").prop('id','formsvalidator_recaptcha_submit_'+formsvalidator_template);
  jQuery(formsvalidator_name).append('<input type="hidden" id="formsvalidator_recaptcha_token" name="recaptcha_token" value="" />');


  jQuery(formsvalidator_name).on("submit",function() {
    fn_formsvalidator_submit();
    return false;
  });
};
function fn_formsvalidator_submit()
{
  var ok=true;
  jQuery( formsvalidator_name).each(function( index ) {
    if(!jQuery(this)[0].checkValidity())
    {
      ok=false;
      //alert(JSON.stringify(jQuery(this)));
      return false;
    }
  });
  if(!ok)
    return false;
  if( jQuery.isFunction(jQuery(this).isValid) && !jQuery(this).isValid() ) {
    return false;
  }
  jQuery(formsvalidator_name+" :submit").append('<div class="ld ld-ring ld-spin"></div>');
  jQuery(formsvalidator_name+" :submit").addClass('running');

  var form = jQuery(formsvalidator_name).get(0);
  var formData = new FormData(form);
  formData.append('action','formvalidator_sendmail');
  formData.append('template',formsvalidator_template);
  formData.append('id_post',formsvalidator_postid);

  if (typeof beforeSend === "function") { 
      beforeSend();
  }

  jQuery.ajax({
    url: formsvalidator_site_url+"/wp-admin/admin-ajax.php",
    type:'POST',
    cache: false,
    contentType: false,
    processData: false,
    dataType: "json",
    data: formData,
    success: function (json) {
      if(json.data.url_dest)
      {
        window.location.href=json.data.url_dest;
        return;
      }
      jQuery(formsvalidator_name+" #msg_feedback").show();
      var msg_error=json.data.msg_retour;
      if(!json.success)
        msg_error='<span style="color:red;">'+msg_error+'</span>';
      if(jQuery(formsvalidator_name+" #msg_feedback").length) // a field already exists
      {
        jQuery(formsvalidator_name+" #msg_feedback").html(msg_error);
      }
      else // insert a new field at the form bottom
      {
        jQuery(formsvalidator_name+" #msg_feedback").remove();
        jQuery(formsvalidator_name).append('<div id="msg_feedback" class="c-form__subtitle">'+msg_error+'</div>');
      }
      jQuery(document).scrollTop( jQuery("#msg_feedback").offset().top-100 );  
      if(json.success)
      {
        if (typeof formsvalidator_success === "function") { 
            formsvalidator_success(formsvalidator_name);
        }
        jQuery(':input',formsvalidator_name)
          .not(':button, :submit, :reset, :hidden')
          .val('')
          .prop('checked', false)
          .prop('selected', false);
        jQuery(formsvalidator_name).get(0).reset();
        if(typeof formsvalidator_sent === "function")
        {
          formsvalidator_sent(msg_error);
        }
      }
      jQuery(formsvalidator_name+" :submit").removeClass('running');
      jQuery(formsvalidator_name+" :submit div.ld").remove();
      return false;
   },
   error: function (json) {
    jQuery(formsvalidator_name+" #msg_feedback").show();
    var msg_error="System failure, try again later";
    if(json!==undefined && json.data!==undefined)
      msg_error=json.data.msg_retour;
    if(jQuery(formsvalidator_name+" #msg_feedback").length) // a field already exists
    {
      jQuery(formsvalidator_name+" #msg_feedback").html('<span style="color:red;">'+msg_error+'</span>');
    }
    else // insert a new field at the form bottom
    {
      jQuery(formsvalidator_name+" #msg_feedback").remove();
      jQuery(formsvalidator_name).append('<div id="msg_feedback" class="c-form__subtitle"><span style="color:red;">'+msg_error+'</span></div>');
    }
    jQuery(document).scrollTop( jQuery("#msg_feedback").offset().top-100 );  
    jQuery(formsvalidator_name+" :submit").removeClass('running');
    jQuery(formsvalidator_name+" :submit div.ld").remove();
    if(typeof formsvalidator_error === "function")
    {
      formsvalidator_error(msg_error);
    }
    return false;
   },
 });
 return false;
}
var formsvalidator_recaptcha_onSubmit = function(token) {
  jQuery(formsvalidator_name+" #formsvalidator_recaptcha_token").val(token);
  fn_formsvalidator_submit();
};